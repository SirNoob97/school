package com.sirnoob.school;

import java.time.LocalDate;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.sirnoob.school.model.Module;
import com.sirnoob.school.model.PersonalData;
import com.sirnoob.school.model.Student;
import com.sirnoob.school.repository.ModuleRepository;
import com.sirnoob.school.repository.StudentRepository;

@DataJpaTest
public class StudentRepositoryTest {

	private final StudentRepository studentRepository;
	private final ModuleRepository moduleRepository;
	
	@Autowired
	public StudentRepositoryTest(StudentRepository studentRepository, ModuleRepository moduleRepository) {
		this.studentRepository = studentRepository;
		this.moduleRepository = moduleRepository;
	}

	@Test
	public void whenFindByModule_thenReturnListStudents() {
		
		Module module = moduleRepository.findByname("Oop");
		
		List<Module> modules = List.of(module);
		
		
		Student student = Student.builder()
									.personalData(PersonalData.builder().dni("Y5088409H").name("Test").lastName("Student")
												.email("email@dot.com").phoneNumber(1234568971L).build())
									.registration(LocalDate.now())
									.schoolYear(1)
									.status("CREATED")
									.modules(modules)
									.build();
				
		studentRepository.save(student);
		
		List<Student> students = studentRepository.findByModules(module);

		students.add(studentRepository.findByPersonalDataDni("456789126"));
		
		Assertions.assertThat(students.size()).isEqualTo(3);
	}
}
