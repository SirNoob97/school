package com.sirnoob.school;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.sirnoob.school.model.Module;
import com.sirnoob.school.repository.ModuleRepository;
import com.sirnoob.school.service.ModuleService;
import com.sirnoob.school.service.ModuleServiceImpl;

@SpringBootTest
public class ModuleServiceTest {

	@Mock
	private ModuleRepository moduleRepository;
	private ModuleService moduleService;

	
//con esto no funciona la inyeccion
//	@Autowired
//	public ModuleServiceTest(ModuleRepository moduleRepository, ModuleService moduleService) {
//		this.moduleRepository = moduleRepository;
//		this.moduleService = moduleService;
//	}

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	
		moduleService = new ModuleServiceImpl(moduleRepository);
		
		Module module = Module.builder().id(1L).name("FOL").build();
		
		Mockito.when(moduleRepository.findById(module.getId())).thenReturn(Optional.of(module));
		Mockito.when(moduleRepository.save(module)).thenReturn(module);
	}
	
	@Test
	public void whenValidateGetId_thenReturnModule() {
		Module found = moduleService.get(1L);
		Assertions.assertThat(found.getName()).isEqualTo("FOL");
	}
	
	@Test
	public void whenValidateUpdateModule_thenReturnModule() {
		Module newModule = moduleService.updateName(1L, "Marckup Languages");
		Assertions.assertThat(newModule.getName()).isEqualTo("Marckup Languages");
	}
}
