package com.sirnoob.school.service;

import com.sirnoob.school.commons.GenericService;
import com.sirnoob.school.model.Module;

public interface ModuleService extends GenericService<Module, Long>{
	
	public Module createModule(Module module);
	
	public Module updateName(Long id, String name);

	public Module deleteModule(Module module);	
	
//	public void deleteModule(Long id);
}
