package com.sirnoob.school.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.sirnoob.school.commons.GenericServiceImpl;
import com.sirnoob.school.model.Module;
import com.sirnoob.school.model.Student;
import com.sirnoob.school.repository.StudentRepository;

@Service
public class StudentServiceImpl extends GenericServiceImpl<Student, Long> implements StudentService {


	private StudentRepository studentRepository;

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	
	@Override
	public JpaRepository<Student, Long> getRepository() {
		return studentRepository;
	}

	@Override
	public List<Student> findByModule(Module module) {
		return studentRepository.findByModules(module);
	}

	@Override
	public Student createStudent(Student student) {
		
		Student studentDB = studentRepository.findByPersonalDataDni(student.getPersonalData().getDni());
		if (studentDB != null)
			return studentDB;

		student.setStatus("CREATED");
		student.setRegistration(LocalDate.now());
		
		studentDB = studentRepository.save(student);
		return studentDB;
	}

	@Override
	public Student updateStudent(Student student) {
		Student studentDB = get(student.getId());

		if (studentDB == null)
			return null;

		studentDB.getPersonalData().setName(student.getPersonalData().getName());
		studentDB.getPersonalData().setLastName(student.getPersonalData().getLastName());
		studentDB.getPersonalData().setEmail(student.getPersonalData().getEmail());
		studentDB.getPersonalData().setPhoneNumber(student.getPersonalData().getPhoneNumber());
		studentDB.setSchoolYear(student.getSchoolYear());

		return studentRepository.save(studentDB);
	}

	@Override
	public Student deleteStudent(Student student) {
		Student studentDB = get(student.getId());

		if (studentDB == null)
			return null;
		
		studentDB.setStatus("DELETED");
		
		return studentRepository.save(studentDB);
	}

	@Override
	public Student updateSchoolYear(Long id, int year) {
		Student studentDB = get(id);
		
		if (studentDB == null)
			return null;
		
		studentDB.setSchoolYear(year);
		
		return studentRepository.save(studentDB);
	}

}
