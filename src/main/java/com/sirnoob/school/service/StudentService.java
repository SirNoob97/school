package com.sirnoob.school.service;

import java.util.List;

import com.sirnoob.school.commons.GenericService;
import com.sirnoob.school.model.Module;
import com.sirnoob.school.model.Student;

public interface StudentService extends GenericService<Student, Long>{

	public Student createStudent(Student student);

	public  Student updateStudent(Student student);

	public Student deleteStudent(Student student);

	public Student updateSchoolYear(Long id, int quantity);
	
	public List<Student> findByModule(Module module);

	//	public void deleteStudent(Long id); //por si hay ganas de algun cambio
}
