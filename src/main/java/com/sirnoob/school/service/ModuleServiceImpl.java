package com.sirnoob.school.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.sirnoob.school.commons.GenericServiceImpl;
import com.sirnoob.school.model.Module;
import com.sirnoob.school.repository.ModuleRepository;

@Service
public class ModuleServiceImpl extends GenericServiceImpl<Module, Long> implements ModuleService {

	private ModuleRepository moduleRepository;

	@Autowired
	public ModuleServiceImpl(ModuleRepository moduleRepository) {
		this.moduleRepository = moduleRepository;
	}

	
	
	
	@Override
	public JpaRepository<Module, Long> getRepository() {
		return moduleRepository;
	}




	@Override
	public Module createModule(Module module) {
		Module moduleDB = moduleRepository.findByname(module.getName());

		if (moduleDB != null)
			return moduleDB;

		module.setStatus("CREATED");

		moduleDB = moduleRepository.save(module);
		return moduleDB;
	}

	@Override
	public Module deleteModule(Module module) {
		Module moduleDB = get(module.getId());

		if (moduleDB == null)
			return null;

		moduleDB.setStatus("DELETED");

		return moduleRepository.save(moduleDB);
	}

	@Override
	public Module updateName(Long id, String name) {
		Module moduleDB = get(id);

		if (moduleDB == null)
			return null;
		
		moduleDB.setName(name);

		return moduleRepository.save(moduleDB);
	}

}
