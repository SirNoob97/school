package com.sirnoob.school.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "students")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Embedded
	@Valid
	private PersonalData personalData;
	private LocalDate registration;

	@Positive(message = "The school year must be positive")
	@Column(name = "school_year", nullable = false)
	private int schoolYear;
	private String status;

	@ManyToMany(cascade = CascadeType.REMOVE)
	@JoinTable(name = "students_modules", joinColumns = {@JoinColumn(name = "students_id")},
	inverseJoinColumns = {@JoinColumn(name = "modules_id")})
	@JsonIgnoreProperties("students")//----------> para evitar la recursion infinita con la peticion GET
	private List<Module> modules;

}
