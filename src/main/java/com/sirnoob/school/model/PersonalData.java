package com.sirnoob.school.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class PersonalData {

	@NotEmpty(message = "The DNI is required")
	@Size(min = 9, max = 9, message = "The DNI must be 9 characters")
	@Column(nullable = false, unique = true)
	private String dni;
	
	@NotEmpty(message = "The name is required")
	@Size(max = 25, message = "The maximum size of the name must be 25 characters")
	@Size(min = 3, message = "The minimun size of the name must be 3 characters")
	@Column(length = 25, nullable = false)
	private String name;

	@NotEmpty(message = "The last name is required")
	@Size(max = 25, message = "The maximum size of the last name must be 25 characters")
	@Size(min = 3, message = "The minumium size of the last name must be 3 characters")
	@Column(name = "last_name", nullable = false, length = 25)
	private String lastName;

	@NotEmpty(message = "The email is required")
	@Email(message = "Invalid email format")
	@Column(unique = true)
	private String email;

	@Positive(message = "The phone number must be positive")
	@Column(name = "phone_number")
	private Long phoneNumber;
}
