package com.sirnoob.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sirnoob.school.model.Module;

@Repository
public interface ModuleRepository extends JpaRepository<Module, Long>{

	public Module findByname(String name);
}
