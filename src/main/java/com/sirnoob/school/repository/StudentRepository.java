package com.sirnoob.school.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sirnoob.school.model.Module;
import com.sirnoob.school.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
	public Student findByPersonalDataDni(String dni);
	public List<Student> findByModules(Module module);
}
