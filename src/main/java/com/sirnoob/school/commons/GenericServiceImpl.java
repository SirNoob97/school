package com.sirnoob.school.commons;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public abstract class GenericServiceImpl<T, ID extends Serializable> implements GenericService<T, ID>{

	public abstract JpaRepository<T, ID> getRepository();
		  	
	@Override
	public T create(T entity) {
		return getRepository().save(entity);
	}
	
	@Override
	public List<T> listAll() {
		return getRepository().findAll();
	}

	@Override
	public T get(ID id) {
		return getRepository().findById(id).orElse(null);
	}

//	este metodo delete como los otros comentados en los servicios de las entidades si borra registros de la base de datos
	@Override
	public void delete(ID id) {
		getRepository().deleteById(id);
	}

}
