package com.sirnoob.school.commons;

import java.io.Serializable;
import java.util.List;

public interface GenericService<T, ID extends Serializable> {

	public T create(T entity);

	public List<T> listAll();
	
	public T get(ID id);

	public void delete(ID id);
	
//	public T update(T entity);
	
//	public T delete(ID id);
	
}
//estos servicios genericos se quedaran asi hasta que encuentre una manera de implementar logica adecuada con ellos
//para las demas clases
