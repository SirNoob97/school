package com.sirnoob.school.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sirnoob.school.model.Module;
import com.sirnoob.school.service.ModuleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/")
public class ModuleController {

	private ModuleService moduleService;

	@Autowired
	public ModuleController(ModuleService moduleService) {
		this.moduleService = moduleService;
	}

	@GetMapping(value = "modules/all")
	public ResponseEntity<List<Module>> listAllModules() {
		List<Module> modules = new ArrayList<>();
		modules = moduleService.listAll();

		if (modules.isEmpty())
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok(modules);
	}

	@GetMapping(value = "modules/{id}")
	public ResponseEntity<Module> getModule(@PathVariable Long id) {
		log.info("Fetching module with id {}", id);
		Module moduleDB = moduleService.get(id);

		if (moduleDB == null) {
			log.error("Module with id: {} not found", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(moduleDB);
	}

	@PostMapping(value = "modules")
	public ResponseEntity<Module> createModule(@Valid @RequestBody Module module, BindingResult result) {
		log.info("Creating Module: {}", module);
		if (result.hasErrors())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));

		Module moduleDB = moduleService.createModule(module);
		return ResponseEntity.status(HttpStatus.CREATED).body(moduleDB);
	}

	@PutMapping(value = "modules/{id}/name")
	public ResponseEntity<Module> updateModuleName(@PathVariable Long id,
			@RequestParam(name = "name", required = true) String name) {
		log.info("Fetching & Updating module with id {}", id);
		Module moduleDB = moduleService.updateName(id, name);

		if (moduleDB == null) {
			log.error("Unable to update. Module with id {} not found.", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(moduleDB);
	}

	@DeleteMapping(value = "modules/{id}")
	public ResponseEntity<Module> deleteModule(@PathVariable Long id) {
		log.info("Fetching module with id {}", id);
		Module moduleDB = moduleService.get(id);

		if (moduleDB == null) {
			log.error("Unable to delete. Module with id {} not found.", id);
			return ResponseEntity.notFound().build();
		}

		moduleDB = moduleService.deleteModule(moduleDB);
		return ResponseEntity.ok(moduleDB);
	}

	private String formatMessage(BindingResult result) {
		List<Map<String, String>> errors = result.getFieldErrors().stream().map(err -> {
			Map<String, String> error = new HashMap<>();
			error.put(err.getField(), err.getDefaultMessage());
			return error;
		}).collect(Collectors.toList());

		ErrorMessage errorMessage = ErrorMessage.builder().code("001").messages(errors).build();
		ObjectMapper mapper = new ObjectMapper();
		String jsonMessage = "";

		try {
			jsonMessage = mapper.writeValueAsString(errorMessage);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonMessage;
	}
}
