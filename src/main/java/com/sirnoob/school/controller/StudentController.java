package com.sirnoob.school.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sirnoob.school.model.Student;
import com.sirnoob.school.service.StudentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/")
public class StudentController {

	private StudentService studentService;

	@Autowired
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}

	@PostMapping(value = "students")
	public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student, BindingResult result) {
		log.info("Creating Student: {}", student);
		if (result.hasErrors())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
		
		Student studentDB = studentService.createStudent(student);
		return ResponseEntity.status(HttpStatus.CREATED).body(studentDB);
	}

	@GetMapping(value = "students/all")
	public ResponseEntity<List<Student>> listAllStudents() {
		List<Student> students = new ArrayList<>();
		students = studentService.listAll();

		if (students.isEmpty())
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok(students);
	}

	@GetMapping(value = "students/{id}")
	public ResponseEntity<Student> getStudent(@PathVariable Long id) {
		log.info("Fetching student with id {}", id);
		Student student = studentService.get(id);

		if (student == null) {
			log.error("Student with id: {} not found", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(student);
	}


	@PutMapping(value = "students/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student) {
		log.info("Fetching student with id {}", id);
		student.setId(id);

		log.info("Updating student with id {}", id);
		Student studentDB = studentService.updateStudent(student);

		if (studentDB == null) {
			log.error("Unable to update. Student with id {} not found.", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(studentDB);
	}

	@DeleteMapping(value = "students/{id}")
	public ResponseEntity<Student> deleteStudent(@PathVariable Long id) {
		log.info("Fetching student with id {}", id);

		Student studentDB = studentService.get(id);

		if (studentDB == null) {
			log.error("Unable to delete. Student with id {} not found.", id);
			return ResponseEntity.notFound().build();
		}

		studentDB = studentService.deleteStudent(studentDB);
		return ResponseEntity.ok(studentDB);
	}

	@PutMapping(value = "students/{id}/year")
	public ResponseEntity<Student> updateSchoolYear(@PathVariable Long id,
			@RequestParam(name = "quantity", required = true) int quantity) {

		log.info("Fetching & Updating student with id {}", id);
		Student studentDB = studentService.updateSchoolYear(id, quantity);

		if (studentDB == null) {
			log.error("Unable to update. Student with id {} not found.", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(studentDB);
	}

	private String formatMessage(BindingResult result) {
		List<Map<String, String>> errors = result.getFieldErrors().stream().map(err -> {
			Map<String, String> error = new HashMap<>();
			error.put(err.getField(), err.getDefaultMessage());
			return error;
		}).collect(Collectors.toList());

		ErrorMessage errorMessage = ErrorMessage.builder().code("001").messages(errors).build();
		ObjectMapper mapper = new ObjectMapper();
		String jsonMessage = "";

		try {
			jsonMessage = mapper.writeValueAsString(errorMessage);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonMessage;
	}
}
