INSERT INTO modules (id, name, status) VALUES (1, 'Oop', 'CREATED');
INSERT INTO modules (id, name, status) VALUES (2, 'Data Bases', 'CREATED');
INSERT INTO modules (id, name, status) VALUES (3, 'Services and Processes', 'CREATED');

INSERT INTO students (id, dni, name, last_name, email, phone_number, status, registration, school_year)
VALUES (1, '456789126', 'Martin','Lopez', 'email@a.com', 1784531289, 'CREATED', '2018-09-05', 1);

INSERT INTO students (id, dni, name, last_name, email, phone_number, status, registration, school_year)
VALUES (2, '789451263', 'Alejandro','Diaz', 'yahoo@a.com', 9517531234, 'CREATED', '2020-09-05', 1);

INSERT INTO students (id, dni, name, last_name, email, phone_number, status, registration, school_year)
VALUES (3, '741123964', 'Nombre','Apellido', 'hola@a.com', 1234567894, 'CREATED', '2008-05-05', 1);

INSERT INTO students_modules (students_id, modules_id) SELECT students.id, modules.id 
FROM students inner join modules ON students.id = 1 AND modules.id = 3;

INSERT INTO students_modules (students_id, modules_id) SELECT students.id, modules.id 
FROM students inner join modules ON students.id = 2 AND modules.id = 2;

INSERT INTO students_modules (students_id, modules_id) SELECT students.id, modules.id 
FROM students inner join modules ON students.id = 3 AND modules.id = 1;